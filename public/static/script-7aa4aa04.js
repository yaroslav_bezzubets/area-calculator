function setup() {
    let uuid = Cookies.get('uuid');
    let uuidElement = document.getElementById('uuid');
    uuidElement.innerHTML = uuid;

    document.getElementById("calculate").addEventListener("click", () => {
        let result = document.getElementById("result");
        result.classList.remove('error');
        let x = document.getElementById("x").value;
        let y = document.getElementById("y").value;
        let u = window.location.href.split('/').at(-1);
        if (x == '') x = '0'
        if (y == '') y = '0';
        fetch("/script-7aa4aa04",
            {headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                },
            method: "POST",
            body: JSON.stringify({x: x, y: y,  u: u})})
            .then((response) => response.json())
            .then((data) => {
                if (data.error) {
                    result.textContent = data.error;
                    result.classList.add('error');
                    return;
                }
            result.textContent = data.value + " " + data.unit;
        }).catch((error) => {
            result.textContent = "Error happened";
            result.classList.add('error');
        });
    });
}

window.onload = setup;

